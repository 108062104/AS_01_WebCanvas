const canvas = document.getElementById("myCanvas");
const ctx = canvas.getContext("2d");
ctx.lineCap = "round";
ctx.lineJoin = "round";
document.body.style.cursor = "url(./src/img/pencil.png) 0 32, auto";

var blank = new Image();
blank.src = canvas.toDataURL();
var original = new Image();
original.src = blank.src;
var last = new Image();
last.src = blank.src;
var next = new Image();
// for undo/redo
let step = -1;
let layers = [];
// for drawing
let drawing = false;
let pencilOn = true;
let eraserOn = false;
let typeOn = false;
let straight = false;
let circle = false;
let triangle = false;
let rectangle = false;
let filled = false;
let highlighting = false;
// for typing
let typing = false;
let typed = false;
let x = 0;
let y = 0;

canvas.addEventListener("mousedown", startDrawing);
canvas.addEventListener("mousemove", drawLine);
window.addEventListener("mouseup", stop);
canvas.addEventListener("mouseup", typingText);

function startDrawing(e) {
    step++;
    while (step < layers.length) {
        layers.pop();
    }
    if(step > 0) {last.src = layers[step-1]; console.log("update"); }
    x = e.offsetX;
    y = e.offsetY;
    if(pencilOn) {
        ctx.lineWidth = document.getElementById("size").value;
        ctx.strokeStyle = document.getElementById("colorpicker").value;
        ctx.fillStyle = document.getElementById("colorpicker").value;
        ctx.globalAlpha = 1;
        ctx.globalCompositeOperation = "source-over";
        if(highlighting) ctx.globalAlpha = 0.3;
        else ctx.globalAlpha = 1;
    } else if (eraserOn) {
        ctx.lineWidth = document.getElementById("size").value;
        ctx.globalAlpha = 1;
        ctx.globalCompositeOperation = "destination-out";
    }
    if(!typeOn) drawing = true;
}

function stay() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.drawImage(original, 0, 0);
}

function drawLine(e) {
    if(!drawing) return;
    ctx.beginPath();
    if(straight) {
        stay();
        ctx.moveTo(x, y);
        ctx.lineTo(e.offsetX, e.offsetY);
        ctx.stroke();
    } else if(rectangle) {
        stay();
        ctx.moveTo(x, y);
        ctx.lineTo(x, e.offsetY);
        ctx.lineTo(e.offsetX, e.offsetY);
        ctx.lineTo(e.offsetX, y);
        ctx.lineTo(x, y);
        ctx.stroke();
    } else if(circle) {
        stay();
        ctx.arc((x+e.offsetX)/2, (y+e.offsetY)/2, 
                Math.sqrt(Math.pow(x-e.offsetX, 2)+Math.pow(y-e.offsetY, 2))/2, 0, 2 * Math.PI);
        ctx.stroke();
    } else if(triangle) {
        stay();
        ctx.moveTo(Math.abs(x+e.offsetX)/2, Math.min(y, e.offsetY));
        ctx.lineTo(x, Math.max(y, e.offsetY));
        ctx.lineTo(e.offsetX, Math.max(y, e.offsetY));
        ctx.lineTo(Math.abs(x+e.offsetX)/2, Math.min(y, e.offsetY));
        ctx.stroke();
    } else { // just pencil
        ctx.moveTo(x, y);
        ctx.lineTo(e.offsetX, e.offsetY);
        ctx.stroke();
        x = e.offsetX;
        y = e.offsetY;
    }
    if(filled) ctx.fill();
    else ctx.closePath();
}

function stop(e) {
    drawLine(e);
    if(drawing || typed) {
        let dataURL = canvas.toDataURL();
        original.src = dataURL;
        layers.push(dataURL);
        console.log("layers++");
        console.log(step);
        drawing = false;
        typed = false;
    }
    x = 0;
    y = 0;
}

function typingText(e) {
    if(typeOn && !typing) {
        typing = true;
        ctx.globalAlpha = 1;
        ctx.globalCompositeOperation = "source-over"
        var input = document.createElement("input");
        input.type = "text";
        input.style.position = 'fixed';
        input.style.left = e.clientX + "px";
        input.style.top = (e.clientY-8) + "px";
        input.onkeydown = listenKeyboard;
        document.body.appendChild(input);
        input.focus();
    }
}

function listenKeyboard(e) {
    if(e.key == "Enter") {
        ctx.textBaseline = 'top';
        ctx.font = document.getElementById("font size").value + "px " + document.getElementById("font").value;
        ctx.fillStyle = document.getElementById("colorpicker").value;
        ctx.fillText(this.value, parseInt(this.style.left, 10)-24, parseInt(this.style.top, 10)-80);
        document.body.removeChild(this);
        typing = false;
        typed = true;
    }
}

function pencil() {
    pencilOn = true;
    eraserOn = typeOn = straight = circle = triangle = rectangle = crop = highlighting = false;
    document.body.style.cursor = "url(./src/img/pencil.png) 0 32, auto";
}

function eraser() {
    eraserOn = true;
    pencilOn = typeOn = straight = circle = triangle = rectangle = crop = highlighting = false;
    document.body.style.cursor = "url(./src/img/eraser.png) 0 32, auto";
}

function to_type() {
    typeOn = true;
    pencilOn = eraserOn = straight = circle = triangle = rectangle = crop = highlighting = false;
    document.body.style.cursor = "url(./src/img/text.png) 0 -16, auto";
}

function resetCanvas() {
    console.log("reset!");
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    step = -1;
    original.src = blank.src;
    while(layers.length > 0) {
        layers.pop();
    }
}

function lastStep() {
    console.log("last!");
    console.log(step);
    if(step >= 0) {
        step--;
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        // update what canvas is showing now
        if(step == -1) {
            original.src = blank.src;
        } else {
            original.src = layers[step];
            ctx.globalAlpha = 1;
            ctx.globalCompositeOperation = "source-over";
            ctx.drawImage(last, 0, 0);
        }
        // update img last
        if(step > 0) last.src = layers[step-1];
        else last.src = blank.src;
        // update img next
        next.src = layers[step+1];
    }
}

function nextStep() {
    console.log("next!");
    console.log(step);
    console.log(layers.length);
    if(step+1 < layers.length) {
        step++;
        // update what canvas is showing now
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        original.src = layers[step];
        ctx.globalAlpha = 1;
        ctx.globalCompositeOperation = "source-over";
        ctx.drawImage(next, 0, 0);
        // update img last
        if(step > 0) last.src = layers[step-1];
        else last.src = blank.src;
        // update img next
        if(step+1 < layers.length) next.src = layers[step+1];
    }
}

function downLoad() {
    let picture = document.getElementById("dl");
    picture.href = canvas.toDataURL();
    picture.download = "unnamed.png";
}

function handleFiles(files) {
    for(var i=0; i<files.length; i++) {
        var file = files[i];
        var reader = new FileReader();
        reader.onload = function(e) {
            var img = new Image();
            img.onload = function() {
                ctx.globalAlpha = 1;
                ctx.globalCompositeOperation = "source-over"
                ctx.drawImage(img, 0, 0, img.width, img.height);
                step++;
                if(step > 0) last.src = layers[step-1];
                let dataURL = canvas.toDataURL();
                original.src = dataURL;
                layers.push(dataURL);
                console.log("layers++");
                console.log(step);
            }
            img.src = e.target.result;
        }
        reader.readAsDataURL(file);
    }
}

function straightLine() {
    straight = pencilOn = true;
    eraserOn = typeOn = circle = triangle = rectangle = crop = highlighting = false;
    document.body.style.cursor = "crosshair";
}

function drawCircle() {
    circle = pencilOn = true;
    eraserOn = typeOn = straight = triangle = rectangle = crop = highlighting = false;
    document.body.style.cursor = "url(./src/img/circle.png) 16 16, auto";
}

function drawTriangle() {
    triangle = pencilOn = true;
    eraserOn = typeOn = straight = circle = rectangle = crop = highlighting = false;
    document.body.style.cursor = "url(./src/img/triangle.png) 12 0, auto";
}

function drawRectangle() {
    rectangle = pencilOn = true;
    eraserOn = typeOn = straight = circle = triangle = crop = highlighting = false;
    document.body.style.cursor = "url(./src/img/rectangle.png) 2 2, auto";
}

function fillOrNot() {
    if(filled) {
        filled = false;
        document.getElementById("fill").src = "./src/img/outlined.png";
    } else {
        filled = true;
        document.getElementById("fill").src = "./src/img/filled.png";
    }
}

function changeWidth() {
    canvas.width = document.getElementById("canvas_w").value;
    ctx.lineCap = "round";
    ctx.lineJoin = "round";
    while(layers.length > 0) layers.pop();
    step = 0;
    ctx.drawImage(original, 0, 0);
    layers.push(original.src);
}

function changeHeight() {
    canvas.height = document.getElementById("canvas_h").value;
    ctx.lineCap = "round";
    ctx.lineJoin = "round";
    while(layers.length > 0) layers.pop();
    step = 0;
    ctx.drawImage(original, 0, 0);
    layers.push(original.src);
}

function highlight() {
     highlighting = pencilOn = true;
     eraserOn = typeOn = straight = circle = rectangle = triangle = crop = false;
     document.body.style.cursor = "url(./src/img/highlighter.png) 0 32, auto";
 }