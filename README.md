# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Fill the area inside the shapes                  | 1~5%      | Y         |
| Change the size of canvas                        | 1~5%      | Y         |
| Highlighter                                      | 1~5%      | Y         |

---

### How to use 

    It's just a canvas! You can use the tools on the right side to draw anything you want.
    - Use pencil and eraser
        - can choose the color and the size
    - Type some words
        - can choose the color, the font and size of the words
    - Draw straight line or some shapes
        - can also choose whether you want the shapes to be filled or not
    - Download your perfect work
    - Upload an existing image in your computer and put it in the canvas
    - Undo or redo
    - Reset the canvas to a blank canvas
    - Resize the canvas
    - Use the special marker (which works kinda like a highlighter)
    All these functions above can be used by clicking the button or the simple menu on the right side bar. Have fun!

### Function description

    - Fill the area inside the shapes: 
        You can choose whether you want to fill it or not, only through a single click on the little square at the last line.
    - Change the size of canvas: 
        At the top of the side bar, there's some options for you to choose the width and height of your canvas. Use it carefully while you're drawing, you would lost the chance to undo or redo. And if you change the size of the canvas and then draw or erase something in the canvas, the rest of the canvas which is cropped would be disappear.
    - Use highlighter: 
        Just choose the special marker at the last line and draw!

### Gitlab page link

    https://108062104.gitlab.io/AS_01_WebCanvas

### Others (Optional)

    This is exhausting but fun for me. Thank you guys.

<style>
table th{
    width: 100%;
}
</style>